"""import either a csv file or a netCDF file and convert them to xr dataset"""

import os.path

import pandas as pd
import xarray as xr

xr.set_options(keep_attrs=True)


def check_file(fn):
    """check the extension of the file you want to load"""
    extension = os.path.splitext(fn)[1]
    return extension


def load_data_csv(fn):
    """create dataframe from csv file and set dates as index, then convert it to xarray dataset.

    Args:
        fn: filename of data file.

    Returns: xarray dataset.
    """
    df = pd.read_csv(fn, parse_dates=["time"], index_col="time")
    data_csv = xr.Dataset.from_dataframe(df)
    xr.set_options(keep_attrs=True)
    # rename t variable
    data_csv["temperature_2_meter"] = data_csv["t"]
    data_csv = data_csv.drop(["t"])

    print("read and transform csv data")

    return data_csv


def load_data_nc(fn):
    """create dataset from netCDF file.

    Args:
        fn: filename of data file.

    Returns: xarray dataset.
    """
    with xr.open_dataset(fn) as ds:
        ds.load()

    print("read nc data")

    return ds


def data_nc_avg(data_nc, lat_min="-90", lat_max="90"):
    """average the t2m over all longitudes and optionally over all latitudes or over selected latitudes.

    Args:
        data_nc: dataset.
        lat_min: minimum latitude for averaging, default value is -90°.
        lat_max: maximum latitude for averaging, default value is 90°.

    Returns: dataset with meridional and zonal averaged 2m temperatures.
    """
    data_nc_av = data_nc.sel(latitude=slice(lat_min, lat_max)).mean(
        ["longitude", "latitude"]
    )
    return data_nc_av
