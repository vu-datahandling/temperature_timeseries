#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
plot temperature timeseries and plot monthly anomalies
"""

import xarray as xr
import matplotlib.pyplot as plt
import numpy as np

xr.set_options(keep_attrs=True)


def save_as_nc(data):
    """save dataset as netcdf file.

    Args:
        data: dataset to be used.

    Returns: netcdf file.
    """
    data.to_netcdf("temperature_timeseries/data/data.nc")
    data.close()
    print("saved dataset to netcdf")


def timeseries(data, anomalies):
    """save timeseries plot as png in the base directory.

    Args:
        data: dataset to be used.
        p: parameters of linear trend
        months: months

    Returns: png file.
    """
    data = data.resample(time="Y").mean(keep_attrs=True)
    anomalies = anomalies.resample(time="Y").mean(keep_attrs=True)
    data["temperature_2_meter"].attrs["units"] = "K"
    anomalies["temperature_2_meter"].attrs["units"] = "K"

    data.temperature_2_meter.plot(label="temperature timeseries")
    anomalies.temperature_2_meter.plot(label="anomalies")
    plt.legend()
    plt.title("Yearly mean temperature and anomaly timeseries")

    plt.savefig("timeseries.png")
    print("saved timeseries plot in base directory")
    plt.clf()
    return


def trend(data):
    """save trend of timeseries data as a plot in base directory.

    Args:
        data: dataset to be used.

    Returns: png file with yearly mean 2m temperatures and a fitted linear trend.
    """
    data = data.resample(time="Y").mean(keep_attrs=True)

    years = data.coords["time"].dt.year
    temps = data.temperature_2_meter.values
    reg = np.polyfit(years, temps, 1)
    p = np.poly1d(reg)
    plt.plot(years, p(years), "r--")
    plt.plot(years, temps, label="trend in yearly mean 2 meter temperature")
    plt.ylabel("yearly mean temperature (K)")
    plt.title("yearly mean 2m temperature and linear trend")

    plt.savefig("trend.png")
    print("saved trend plot in base directory")
    plt.clf()
    return
