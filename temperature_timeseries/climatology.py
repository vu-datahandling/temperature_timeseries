#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
generate climatology (monthly means for 1960-1990 reference period) and generate anomaly temperatures
"""
import xarray as xr
import numpy as np

xr.set_options(keep_attrs=True)


def climatology(data):
    """generate a dataset with monthly mean temperature values for 1960-1990 reference period.

    Args:
        data: name of the xarray dataset to be used.

    Returns: dataset with a temperature value for every month (12 in total).
    """
    clim = (
        data.sel(time=slice("1960-01-01", "1989-12-31"))
        .temperature_2_meter.groupby("time.month")
        .mean(keep_attrs=True)
    )
    return clim


def anomalies(data, clim):
    """subtract monthly mean temperatures from temperature timeseries to get anomalies.

    Args:
        data: name of xarray dataset to be used.
        clim: xarray dataset with monthly mean temperatures (climatology).

    Returns: anomaly temperature timeseries.
    """
    anomalies = data.groupby("time.month") - clim
    return anomalies
