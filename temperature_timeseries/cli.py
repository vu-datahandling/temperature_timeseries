import argparse

from . import loading, climatology, plotting


def temperature_timeseries():
    """Entry point for the command line interface"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--filename",
        required=True,
        help="filename you want to read",
    )
    parser.add_argument(
        "--lat_min",
        required=False,
        help="minimum latitude for averaging, default -90°",
    )
    parser.add_argument(
        "--lat_max",
        required=False,
        help="maximum latitude for averaging, default 90°",
    )
    parser.add_argument(
        "--nc",
        required=False,
        help="if true, save file as netcdf",
    )
    parser.add_argument(
        "--trend",
        required=False,
        help="if true, add trend line to plot",
    )

    args = parser.parse_args()

    extension = loading.check_file(args.filename)

    if extension == ".nc":
        data = loading.load_data_nc(args.filename)
        if args.lat_min is not None:
            data = loading.data_nc_avg(data, args.lat_min, args.lat_max)

    if extension == ".csv":
        data = loading.load_data_csv(args.filename)

    if args.trend is not None:
        plotting.trend(data)

    clim = climatology.climatology(data)
    anomalies = climatology.anomalies(data, clim)

    plotting.timeseries(data, anomalies)

    if args.nc is not None:
        plotting.save_as_nc(data)
